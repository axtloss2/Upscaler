# Press
A collection of content mentioning Upscaler from various writers, content creators, etc.

## Articles
- [Use ‘Upscaler’ to Enhance Low-Res Images with AI on Linux](https://www.omgubuntu.co.uk/2022/11/upscaler-open-source-ai-image-upscale-app-for-linux)
- [Fotos verbessern mit Upscaler](https://gnulinux.ch/fotos-verbessern-mit-upscaler)
