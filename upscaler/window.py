# window.py: main window
#
# Copyright (C) 2022 Hari Rana / TheEvilSkeleton
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only


import os
import subprocess
import re
from os.path import basename
from gi.repository import Adw, Gtk, GLib, Gdk
from upscaler.dialog_upscaling import UpscalingDialog
from upscaler.threading import RunAsync
from upscaler.file_chooser import FileChooser

@Gtk.Template(resource_path='/io/gitlab/theevilskeleton/Upscaler/gtk/window.ui')
class UpscalerWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'UpscalerWindow'

    """ Declare child widgets. """
    toast = Gtk.Template.Child()
    stack_upscaler = Gtk.Template.Child()
    button_input = Gtk.Template.Child()
    action_image_size = Gtk.Template.Child()
    action_upscale_image_size = Gtk.Template.Child()
    button_upscale = Gtk.Template.Child()
    spinner_loading = Gtk.Template.Child()
    image = Gtk.Template.Child()
    # video = Gtk.Template.Child()
    combo_models = Gtk.Template.Child()
    string_models = Gtk.Template.Child()
    # spin_scale = Gtk.Template.Child()
    button_output = Gtk.Template.Child()
    label_output = Gtk.Template.Child()

    """ Initialize function. """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        """ Declare default models. """
        self.model_images = {
            'realesrgan-x4plus': _('Photo'),
            'realesrgan-x4plus-anime': _('Cartoon/Anime'),
        }

        """ Display models. """
        for model in self.model_images.values():
            self.string_models.append(model)

        """ Connect signals. """
        self.button_input.connect('clicked', self.__open_file)
        self.button_upscale.connect('clicked', self.__upscale)
        self.button_output.connect('clicked', self.__output_location)
        self.combo_models.connect('notify::selected', self.__set_model)
        # self.spin_scale.connect('value-changed', self.__update_post_upscale_image_size)

        # self.model_videos = [
        #     'realesr-animevideov3',
        # ]

        """ Declare variables. """
        self.upscale_dialog = None

    """ Open file and display it if the user selected it. """
    def __open_file(self, *args):
        FileChooser.open_file(self)

    """ Select output file location. """
    def __output_location(self, *args):
        FileChooser.output_file(self)

    """ Update progress. """
    def __upscale_progress(self, progress):
        if self.upscale_dialog:
            self.upscale_dialog.set_progress(progress)

    def __upscale(self, *args):

        """ Since GTK is not thread safe, prepare some data in the main thread. """
        self.upscale_dialog = UpscalingDialog(self)

        """ Run in a separate thread. """
        def run():
            command = ['realesrgan-ncnn-vulkan',
                       '-i', self.input_file_path,
                       '-o', self.output_file_path,
                       '-n', list(self.model_images)[self.combo_models.get_selected()],
                       '-s', '4',
                       ]
            process = subprocess.Popen(command, stderr=subprocess.PIPE, universal_newlines=True)
            print('Running: ', end='')
            print(*command)
            """ Read each line, query the percentage and update the progress bar. """
            for line in iter(process.stderr.readline, ''):
                print(line, end='')
                res = re.match('^(\d*.\d+)%$', line)
                if res:
                    GLib.idle_add(self.__upscale_progress, float(res.group(1)))

        """ Run when run() function finishes. """
        def callback(*args):
            self.upscale_dialog.close()
            self.upscale_dialog = None
            self.upscaling_completed_dialog()

        """ Run functions asynchronously. """
        RunAsync(run, callback)
        self.upscale_dialog.present()

    """ Ask the user if they want to open the file. """
    def upscaling_completed_dialog(self, *args):
        def response(_widget):
            path = f'file://{self.output_file_path}'
            Gtk.show_uri(self, path, Gdk.CURRENT_TIME)

        toast = Adw.Toast.new(_('Image upscaled'))
        toast.set_button_label(_('Open'))
        toast.connect('button-clicked', response)
        self.toast.add_toast(toast)

    """ Set model and print. """
    def __set_model(self, *args):
        print(_('Model name: {}').format(list(self.model_images)[self.combo_models.get_selected()]))

    """ Update post-upscale image size as the user adjusts the spinner. """
    # def __update_post_upscale_image_size(self, *args):
    #     upscale_image_size = [
    #         self.image_size[1] * int(self.spin_scale.get_value()),
    #         self.image_size[2] * int(self.spin_scale.get_value()),
    #     ]
    #     self.action_upscale_image_size.set_subtitle(f'{upscale_image_size[0]} × {upscale_image_size[1]}')
